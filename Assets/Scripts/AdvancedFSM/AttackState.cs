using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackState : FSMState
{
    public AttackState(Transform[] wp) 
    { 
        waypoints = wp;
        stateID = FSMStateID.Attacking;

		sceneGizmoColor = Color.red;

		curRotSpeed = 15;
	}

    public override void Reason(List<Transform> targets, Transform target, NPCTankController npc)
    {
		if (target == null)
		{
			npc.GetComponent<NPCTankController>().SetTransition(Transition.LostPlayer);
			Debug.Log("Switch to Patrol State");
			targets.RemoveAll(item => item == null);
			return;
		}
		float dist = Vector3.Distance(npc.transform.position, target.position);
		if (dist >= 15.0f)
		{
			Debug.Log("Switch to Patrol State");
			npc.GetComponent<NPCTankController>().SetTransition(Transition.LostPlayer);
		}
		
	}

    public override void Act(List<Transform> targets, Transform target, NPCTankController npc)
    {
        //Set the target position
        destPos = target.position;

		//Always Turn the turret towards the player
		Transform turret = npc.GetComponent<NPCTankController>().turret;
		Quaternion turretRotation = Quaternion.LookRotation(destPos - turret.position);
		turret.rotation = Quaternion.Slerp(turret.rotation, turretRotation, Time.deltaTime * curRotSpeed);

		//Shoot bullet towards the player
		npc.navmeshAgent.isStopped = true;
		npc.GetComponent<NPCTankController>().ShootBullet();
    }

	public override void DoBeforeEntering(NPCTankController npc)
	{

	}

	public override void DoBeforeLeaving(NPCTankController npc)
	{

	}
}
