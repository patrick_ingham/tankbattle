using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChaseState : FSMState
{
    public ChaseState(Transform[] wp) 
    { 
        waypoints = wp;
        stateID = FSMStateID.Chasing;

		sceneGizmoColor = Color.magenta;

	}

    public override void Reason(List<Transform> targets, Transform target, NPCTankController npc)
    {
        //Set the target position as the player position
        destPos = target.position;

        //Check the distance with player tank
        //When the distance is near, transition to attack state
       float dist = Vector3.Distance(npc.transform.position, destPos);
       if (dist < 8.5f)
        {
            Debug.Log("Switch to Attack state");
            npc.GetComponent<NPCTankController>().SetTransition(Transition.ReachPlayer); 
        }
        //Go back to patrol is it become too far
        else if (dist >= 40.0f)
        {
            Debug.Log("Switch to Patrol state");
            npc.GetComponent<NPCTankController>().SetTransition(Transition.LostPlayer);
        }
	   else if (target == null) {
			Debug.Log("Switch to Patrol state");
			npc.GetComponent<NPCTankController>().SetTransition(Transition.LostPlayer);
		}
    }

    public override void Act(List<Transform> targets, Transform target, NPCTankController npc)
    {
		destPos = target.position;

		//Always Turn the turret towards the target
		Transform turret = npc.GetComponent<NPCTankController>().turret;
		Quaternion turretRotation = Quaternion.LookRotation(destPos - turret.position);
		turret.rotation = Quaternion.Slerp(turret.rotation, turretRotation, Time.deltaTime * curRotSpeed);


		npc.navmeshAgent.SetDestination(destPos);
		npc.navmeshAgent.isStopped = false;
	}

	public override void DoBeforeEntering(NPCTankController npc)
	{
		npc.navmeshAgent.speed = 5.5f;
	}

	public override void DoBeforeLeaving(NPCTankController npc)
	{

	}
}
