using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatrolState : FSMState
{
    public PatrolState(Transform[] wp) 
    { 
        waypoints = wp;
        stateID = FSMStateID.Patrolling;

		FindNextPoint();

		curRotSpeed = 5;

		sceneGizmoColor = Color.green;
	}

    public override void Reason(List<Transform> targets, Transform target, NPCTankController npc)
    {
		RaycastHit hit;

		Debug.DrawRay(npc.turret.transform.position, npc.turret.transform.forward.normalized * 25, Color.green);

		if (Physics.SphereCast(npc.turret.transform.position, 10, npc.turret.transform.forward, out hit, 25)
			&& hit.collider.CompareTag("tank"))
		{
			Debug.Log("Switch to Chase State");
			npc.currentTarget = hit.transform;
			npc.SetTransition(Transition.SawPlayer);
		}

		//Find another random patrol point if the current point is reached
		else if (Vector3.Distance(npc.transform.position, destPos) <= 5.0f)
		{
			Debug.Log("Switch to Scan State");
			npc.SetTransition(Transition.LookAround);
		}
	}

	public override void Act(List<Transform> targets, Transform player, NPCTankController npc)
	{
		

		//Always Turn the turret towards the front of tank
		Transform turret = npc.turret;
		Quaternion turretRotation = Quaternion.LookRotation(npc.transform.forward, npc.transform.up);
		turret.rotation = Quaternion.Slerp(turret.rotation, turretRotation, Time.deltaTime * curRotSpeed);


		npc.navmeshAgent.SetDestination(destPos);
		npc.navmeshAgent.isStopped = false;
	}

	public override void DoBeforeEntering(NPCTankController npc)
	{
		npc.navmeshAgent.speed = 3.5f;
		FindNextPoint();
	}

	public override void DoBeforeLeaving(NPCTankController npc)
	{
		
	}


}