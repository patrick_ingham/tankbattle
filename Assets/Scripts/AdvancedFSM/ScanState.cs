using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScanState : FSMState
{
	private bool isScanning = true;
	private Quaternion startRotation;
	private int counter = 0;

    public ScanState() //Transform[] wp) 
    { 
       // waypoints = wp;
        stateID = FSMStateID.Scanning;

		sceneGizmoColor = Color.yellow;

		curRotSpeed = 50;
	}

    public override void Reason(List<Transform> targets, Transform target, NPCTankController npc)
    {
		RaycastHit hit;

		Debug.DrawRay(npc.turret.transform.position, npc.turret.transform.forward.normalized * 25, Color.green);

		if (Physics.SphereCast(npc.turret.transform.position, 10, npc.turret.transform.forward, out hit, 25)
			&& hit.collider.CompareTag("tank"))
		{
			Debug.Log("Switch to Chase State");
			npc.currentTarget = hit.transform;
			npc.SetTransition(Transition.SawPlayer);
		}
		else if (counter > 240)
		{
			Debug.Log("Switch to Patrol State");
			npc.SetTransition(Transition.LostPlayer);

		}
	}

    public override void Act(List<Transform> targets, Transform target, NPCTankController npc)
    {
		counter++;
		if (isScanning)
		{
			Transform turret = npc.GetComponent<NPCTankController>().turret;
			turret.Rotate(Vector3.up * curRotSpeed * Time.deltaTime);
		}

		
		npc.navmeshAgent.isStopped = true;
		
    }

	public override void DoBeforeEntering(NPCTankController npc)
	{
		isScanning = true;
		Transform turret = npc.GetComponent<NPCTankController>().turret;
		startRotation = turret.rotation;
	}

	public override void DoBeforeLeaving(NPCTankController npc)
	{
		counter = 0;
		isScanning = false;
	}
}
