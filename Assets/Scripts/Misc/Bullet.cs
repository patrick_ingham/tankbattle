﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    //Explosion Effect
    public GameObject Explosion;

    public float Speed = 30.0f;
    public float LifeTime = 3.0f;
    public int damage = 50;

    void Start()
    {
        Destroy(gameObject, LifeTime);
		this.GetComponent<Rigidbody>().AddForce(this.transform.forward * Speed, ForceMode.Impulse);
    }

    

   /* void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.contacts[0];
		//Instantiate(Explosion, contact.point, Quaternion.identity);
		if (collision.gameObject.tag == "tank")
		{
			NPCTankController[] npcs = FindObjectsOfType<NPCTankController>() as NPCTankController[];
			foreach (NPCTankController npc in npcs) { 
				npc.RemoveFromList(collision.gameObject);
			}
			collision.gameObject.;
		}
		Destroy(gameObject);
    }*/
}